//Messages related actions
//File to define actions

import { SEND_MESSAGE, REPLY_MESSAGE } from './actionTypes';

export const sendMessage = (messageObject) => {
    return {
        type: SEND_MESSAGE,
        payload: messageObject
    };
};

// export const handleTextChange = (key) => (value) => {
//     return {
//         type: HANDLE_TEXT_CHANGE,
//         textKey: key,
//         value: value
//     };
// };

export const replyMessage = (rMessageObject) => {
    return {
        type: REPLY_MESSAGE,
        payload: rMessageObject
    };
};

// export const stopChangingText = () => {
//     return {
//         type: STOP_CHANGING_TEXT
//     };
// };