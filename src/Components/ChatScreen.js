import React, {Component} from 'react';
import {
    View,
    Text,
    FlatList,
    ImageBackground
} from 'react-native';
import Header from './Header';
import Footer from './Footer';
import ChatDate from "./ChatDate";
import {replyMessage, sendMessage} from "../store/actions/index";
import {connect} from "react-redux";

class ChatScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            message: '',
            //messageList: [],
            //answerList: [],
            //isYourTurn: true,
            answerOptions: ["Have a nice day", "Whats up?", "How are you today?"],
            isTyping: false
        };
    }


    render() {
        //let answers = ["Have a nice day", "Whats up?", "How are you today?"];
        // let {height, width} = Dimensions.get('window');
        // var first;

        return (
            <View style={{
                height: '100%',
                width: '100%',
                backgroundColor: '#dfdfff',
                marginBottom: 32,
                alignItems: 'center'
            }}>

                <ImageBackground
                    source={require('../res/Images/background.jpg')}
                    style={styles.bgImage}>

                    <Header
                        isTyping={this.state.isTyping}
                    />

                    <ChatDate />

                    <FlatList
                        style={styles.messageList}
                        data={this.props.messageList}
                        renderItem={this.renderRow}
                        keyExtractor={(item, index) => index.toString()}
                    />

                    <Footer
                        message={this.state.message}
                        handleChange={this.handleChange}
                        sendMessage={this.sendMessage}
                        onEndEditing={this.onEndEditing}
                    />

                </ImageBackground>

            </View>
        );
    }


    handleChange = key => value => {
        this.setState({[key]: value});
        //value === '' ? this.setState({[key]: value}) : this.setState({[key]: value, isTyping: true})
    };

    sendMessage = () => {
        if (this.state.message.length > 0) {

            let text = this.state.message;
            let time = new Date();
            let m = {
                text: text,
                fromUser: 0,
                time: time
            }

            this.props.onSendMessage(m);
            this.setState({
                message: ''
            });
            setTimeout(() => {
                this.setState({isTyping: true});
            }, 500);

            let random = Math.floor(Math.random() * 10) % 3;
            let rText = this.state.answerOptions[random];
            let rDate = new Date();
            let rm = {
                text: rText,
                fromUser: 1,
                time: rDate
            }

            setTimeout(this.replyMessage, 2000, rm);
        }
    };

    replyMessage = (rm) => {
        this.setState({isTyping: false});
        this.props.onReplyMessage(rm);
        // if (!this.state.isYourTurn) {
        //
        // }
    };

    // convertTime = (time) => {
    //     let date = new Date(time);
    //     let date2 = new Date();
    //     let result = (date.getHours() < 10 ? '0' : '') + date.getHours() + ':';
    //     result += (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    //     if (date2.getDay() !== date.getDay()) {
    //         result = date.getDay() + ' ' + date.getMonth() + ' ' + result;
    //     }
    //     return result;
    // }

    // getMessage = () => {
    //     let m = this.state.message;
    //     return m;
    // }

    renderRow = ({item}) => {
        return (
            <View style={{
                flexDirection: 'row',
                maxWidth: '60%',
                minWidth: '20%',
                alignSelf: item.fromUser === 0 ? 'flex-end' : 'flex-start',
                backgroundColor: item.fromUser === 0 ? '#00897b' : '#7cb342',
                //alignSelf: 'flex-end',
                //alignSelf: 'flex-start',
                //backgroundColor: '#00897b',
                borderRadius: 6,
                marginBottom: 8
            }}>
                <Text style={{
                    color: '#ffffff',
                    padding: 7,
                    fontSize: 16
                }}>
                    {item.text}
                </Text>
                <Text style={{
                    color: '#eee',
                    fontSize: 12,
                    padding: 3
                }}>
                    {item.time.getHours() + ":" + item.time.getMinutes() + ":" + item.time.getSeconds()}
                </Text>
            </View>
        );
    };

    // onEndEditing = () => {
    //     this.setState({isTyping: false})
    // }

}

// ChatScreen.defaultProps = {
//     name: 'john'
// }

const mapStateToProps = state => {
    return {
        messageList: state.messages.messageList
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSendMessage: (m) => dispatch(sendMessage(m)),
        onReplyMessage: (m) => dispatch(replyMessage(m))
    };
};

const styles = {
    bgImage: {
        width: '100%',
        height: '100%',
        resizeMode: 'repeat'
    },
    messageList: {
        height: '100%',
        paddingRight: 16,
        paddingLeft: 16
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);