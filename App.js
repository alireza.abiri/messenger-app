import React, {Component} from 'react';
import ChatScreen from './src/Components/ChatScreen';
import {Provider} from "react-redux";
import configureStore from "./src/store/ConfigureStore";

type Props = {};
const store = configureStore();

class App extends Component<Props> {

    render() {
        return (
            <Provider store={store}>
                <ChatScreen/>
            </Provider>
        );
    }
}

export default App;