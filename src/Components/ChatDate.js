import React, {Component} from 'react';
import { View, Text } from 'react-native';

class ChatDate extends Component {
    render() {
        let today = new Date();
        let cDate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        return(
            <View style={styles.bgDate}>
                <Text>{cDate}</Text>
            </View>
        );
    }
}

const styles = {
    bgDate: {
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        marginTop: 16,
        padding: 8,
        borderRadius: 25,
        alignSelf: 'center'
    }
}

export default ChatDate;