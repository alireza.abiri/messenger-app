import {SEND_MESSAGE, REPLY_MESSAGE} from '../actions/actionTypes';

const initialState = {
    messageList: []
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEND_MESSAGE:
        case REPLY_MESSAGE:

            // let today = new Date();
            // //let cTime = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            // let newMessage = {
            //     text: action.payload.text,
            //     fromUser: 0, //0 means you
            //     time: today
            // };
            return Object.assign({}, state, {
                messageList: [...state.messageList, action.payload]
            });

        default:
            return state;
    }
};

export default messagesReducer;