import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';

class Header extends Component {

    showName = () => {
        if (this.props.isTyping) {
            return (
                <View style={styles.titleContainer}>
                    <TouchableOpacity>
                        <Image source={require('../res/Images/lock.png')}
                               style={styles.privateImage}/>
                    </TouchableOpacity>

                    <View style={styles.nameContainer}>
                        <Text style={styles.headerTextStyle}>
                            Hello, John!
                        </Text>
                        <Text>
                            typing...
                        </Text>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={styles.titleContainer}>
                    <Image source={require('../res/Images/lock.png')}
                           style={styles.privateImage}/>
                    <Text style={styles.headerTextStyle}>
                        Hello, John!
                    </Text>
                </View>
            );
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity>
                    <Image
                        style={styles.backButtonPic}
                        source={require('../res/Images/back.png')}
                    />
                </TouchableOpacity>
                {this.showName()}
                <Image
                    style={styles.profilePicStyle}
                    source={require('../res/Images/profile_pic.jpg')}
                />
            </View>
        );
    }
}

const styles = {
    container: {
        flexDirection: 'row',
        height: 60,
        backgroundColor: '#f8f8f8',
        alignItems: 'center',
        justifyContent: 'space-between',
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 10},
        shadowOpacity: 0.2,
        position: 'relative',
        elevation: 12
    },
    headerTextStyle: {
        color: '#939393',
        fontSize: 18
    },
    titleContainer: {
        flexDirection: 'row',
        marginLeft: 50,
        justifyContent: 'center'
    },
    nameContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        marginLeft: 4,
    },
    profilePicStyle: {
        width: 80,
        height: 80,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 10},
        shadowOpacity: 0.2,
        position: 'relative',
        marginBottom: -50,
        marginRight: 32,
        borderRadius: 40,
        borderColor: '#f8f8f8',
        borderWidth: 2
    },
    backButtonPic: {
        width: 24,
        height: 24,
        marginLeft: 32
    },
    privateImage: {
        width: 20,
        height: 20,
        marginTop: 2,
        marginRight: 4
    }
}

export default Header;