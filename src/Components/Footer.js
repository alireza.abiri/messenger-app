import React, {Component} from 'react';
import {Image, TextInput, TouchableOpacity, View} from 'react-native';

class Footer extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.buttonStyle}
                >
                    <Image source={require('../res/Images/emoji.png')}
                           style={styles.imageStyle}
                    />
                </TouchableOpacity>
                <TextInput
                    value={this.props.message}
                    onChangeText={this.props.handleChange('message')}
                    onEndEditing={this.props.onEndEditing}
                    placeholder={"Type your message here..."}
                    style={styles.textInputStyle}
                >
                </TextInput>
                <TouchableOpacity
                    style={styles.buttonStyle}
                    onPress={this.props.sendMessage}
                >
                    <Image source={require('../res/Images/send.png')}
                           style={styles.imageStyle}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    container: {
        bottom: 0,
        backgroundColor: 'rgba(52, 52, 52, 0.0)',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonStyle: {
        shadowColor: '#000',
        shadowOffset: {height: -30, width: 0},
        shadowOpacity: 0.2,
        shadowRadius: 0.5,
        elevation: 8,
        marginBottom: 16,
        marginLeft: 4,
        marginRight: 4,
        padding: 8,
        borderColor: '#eee',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 25
    },
    textInputStyle: {
        shadowColor: '#000',
        shadowOffset: {height: -30, width: 0},
        shadowOpacity: 0.2,
        shadowRadius: 0.5,
        elevation: 8,
        marginRight: 4,
        marginLeft: 4,
        paddingLeft: 16,
        paddingRight: 16,
        borderWidth: 1,
        borderColor: '#eee',
        backgroundColor: '#fff',
        width: '60%',
        marginBottom: 16,
        borderRadius: 25
    },
    imageStyle: {
        width: 24,
        height: 24,
        paddingBottom: 5
    }
}

export default Footer;