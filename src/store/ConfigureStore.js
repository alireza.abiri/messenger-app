import { createStore, combineReducers } from 'redux';

import messagesReducer from './reducers/messages';

//Combines all reducers
const rootReducer = combineReducers({
    messages: messagesReducer
});

//CreateStore needs a reducer to change the states
const configureStore = () => {
    return createStore(rootReducer);
};

export default configureStore;